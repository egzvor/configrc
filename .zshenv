if [[ -o login ]]; then
    export scripts=/home/egzvor/scripts
    export MANPAGER="vim -c 'set nolist' -M +MANPAGER -"
    export TERMCMD='urxvt'
    typeset -U path
    export path=(~/.local/bin ~/.cargo/bin ~/go/bin ~/.gem/ruby/2.6.0/bin/ "$path[@]")
    export cdpath=($HOME $HOME/CloudLinux $HOME/Imunify360)
    export WORKON_HOME=$HOME/.virtualenvs
fi
