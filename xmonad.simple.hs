import System.IO
import System.Exit

import XMonad
import XMonad.Actions.CycleWS
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.DynamicLog

import qualified XMonad.StackSet as W
import qualified Data.Map as M

modm = mod4Mask
myTerminal = "urxvtc"
myLauncher = "/home/egzvor/scripts/rofi_run"
-- myLauncher
--   = "dmenu_run -fn 'Tamzen-10' -nf '#fff' -b -y '0' -x '420' -w '600' -h '27' -p ' Search '"

myKeys :: XConfig Layout -> M.Map (KeyMask, KeySym) (X ())
myKeys conf@(XConfig { XMonad.modMask = modMask }) =
  M.fromList
    $  [ ((modm, xK_Return), spawn myTerminal)
       , ((modm, xK_d)     , spawn myLauncher)
       , ((modm, xK_Tab)   , toggleWS)
       , ( (modm, xK_j)
         , windows W.focusDown
         ) -- %! Move focus to the next window
       , ((modm, xK_k), windows W.focusUp)
       , ( (modm, xK_space)
         , sendMessage NextLayout
         ) -- %! Rotate through the available layout algorithms
       , ( (modm .|. shiftMask, xK_space)
         , setLayout $ XMonad.layoutHook conf
         ) -- %!  Reset the layouts on the current workspace to default
       , ( (modm, xK_t)
         , withFocused $ windows . W.sink
         ) -- %! Push window back into tiling
       , ( (modm, xK_h)
         , sendMessage Shrink
         ) -- %! Shrink the master area
       , ( (modm, xK_l)
         , sendMessage Expand
         ) -- %! Expand the master area
       , ( (modm .|. shiftMask, xK_c)
         , kill
         ) -- %! Close the focused window
       , ( (modm, xK_q)
         , broadcastMessage ReleaseResources >> restart "xmonad" True
         ) -- %! Restart xmonad
       , ( (modm .|. shiftMask, xK_q)
         , io (exitWith ExitSuccess)
         )
       ]
    ++ [ ((m .|. modMask, k), windows $ f i) -- mod-[1..9], Switch to workspace N
       | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
       , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]
       ] -- mod-shift-[1..9], Move client to workspace N

main = xmonad defaults

defaults = docks $ defaultConfig { modMask           = modm
                                 , terminal          = "urxvtc"
                                 , focusFollowsMouse = False
                                 , keys              = myKeys
                                 }
