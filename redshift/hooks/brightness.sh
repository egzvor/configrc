#!/bin/bash
# Set brightness via ddcutil when redshift status changes

set -o pipefail

# Set brightness values for each status.
# Range from 1 to 100 is valid
brightness_day=100
brightness_transition=50
brightness_night=0

hdmi_edid="00ffffffffffff0009d1248045540000201d0103803c2278263f05a9544ca1260e5054a56b80818081c08100a9c0b300d1c001010101565e00a0a0a029503020350055502100001a000000ff0035384b30323030393031390a20000000fd00324c1e591b000a202020202020000000fc0042656e51204c43440a202020200189"

set_brightness() {
    ddcutil --edid "$hdmi_edid" setvcp --noverify 10 "$1"
}

if [ "$1" = period-changed ]; then
    case $3 in
        night)
            set_brightness $brightness_night
            ;;
        transition)
            set_brightness $brightness_transition
            ;;
        daytime)
            set_brightness $brightness_day
            ;;
    esac
fi
