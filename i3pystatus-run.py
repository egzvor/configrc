#!/home/egzvor/.local/share/virtualenvs/i3pystatus/bin/python

from i3pystatus import Status

# flake8: noqa: E128

status = Status()

DRACULA = {
    "background": "#282a36",
    "current line": "#44475a",
    "selection": "#44475a",
    "foreground": "#f8f8f2",
    "comment": "#6272a4",
    "cyan": "#8be9fd",
    "green": "#50fa7b",
    "orange": "#ffb86c",
    "pink": "#ff79c6",
    "purple": "#bd93f9",
    "red": "#ff5555",
    "yellow": "#f1fa8c",
}

APPRENTICE = {
    "foreground": "#bcbcbc",
    "background": "#1c1c1c",
    "grey15": "#262626",
    "grey": "#444444",
    "dark_purple": "#5f5f87",
    "green": "#5f875f",
    "cyan": "#5f8787",
    "blue": "#5f87af",
    "moderate_cyan": "#5fafaf",
    "light_grey": "#6c6c6c",
    "dark_yellow": "#87875f",
    "purple": "#8787af",
    "light_green": "#87af87",
    "light_blue": "#8fafd7",
    "red": "#af5f5f",
    "white_grey": "#bcbcbc",
    "orange": "#ff8700",
    "yellow": "#ffffaf",
    "white": "#ffffff",
}

SOLARIZED = {
    "background": "#fdf6e3",
    "foreground": "#657b83",
    "fadeColor": "#fdf6e3",
    "cursorColor": "#586e75",
    "pointerColorBackground": "#93a1a1",
    "pointerColorForeground": "#586e75",
    "black": "#073642",
    "light_black": "#002b36",
    "red": "#dc322f",
    "light_red": "#cb4b16",
    "green": "#859900",
    "light_green": "#586e75",
    "yellow": "#b58900",
    "light_yellow": "#657b83",
    "blue": "#268bd2",
    "light_blue": "#839496",
    "magenta": "#d33682",
    "light_magenta": "#6c71c4",
    "cyan": "#2aa198",
    "light_cyan": "#93a1a1",
    "white": "#eee8d5",
    "light_white": "#fdf6e3",
}

COLORSCHEME = SOLARIZED

status.register("clock",
        format="%H:%M %b/%d/%Y",
        color=COLORSCHEME['foreground'],
        )

status.register("network",
        interface="wlp2s0",
        format_up="\ue806",
        color_up=COLORSCHEME["foreground"],
        color_down=COLORSCHEME["red"],
        dynamic_color=False,
        )

# status.register("openvpn_simple",
#         format="{status}",
#         color_up=COLORSCHEME['blue'],
#         color_down=COLORSCHEME['foreground'],
#         status_up="\ue803",
#         status_down="\ue804",
#         vpn_name="protonvpn",
#         )

status.register("battery",
    format="{status} {percentage:.0f}%",
    alert=True,
    alert_percentage=10,
    alert_timeout=10,
    color=COLORSCHEME["yellow"],
    charging_color=COLORSCHEME["blue"],
    full_color=COLORSCHEME["foreground"],
    status={
        "DIS": "\ue818",
        "CHR": "\ue818",
        "FULL": "\ue817",
    },
    levels={
        25: "\ue819",
        50: "\ue815",
        75: "\ue816",
    },
    )

status.register("disk",
        path="/",
        hints = {"markup": "pango"},
        format="{avail} GB",
        color=COLORSCHEME["foreground"],
        )

status.register("mem",
        format=" {avail_mem}GB",
        divisor=1024**3,
        color=COLORSCHEME["foreground"],
        warn_percentage=60,
        warn_color=COLORSCHEME["yellow"],
        alert_color=COLORSCHEME["red"],
        )

# Shows your CPU temperature, if you have a Intel CPU
status.register("temp",
    format="\ue802 {Package_id_0:.0f}°C",
    lm_sensors_enabled=True,
    color=COLORSCHEME["foreground"],
    alert_color=COLORSCHEME["red"],
    alert_temp=60,
    )

# Shows the address and up/down state of eth0. If it is up the address is shown in
# green (the default value of color_up) and the CIDR-address is shown
# (i.e. 10.10.10.42/24).
# If it's down just the interface name (eth0) will be displayed in red
# (defaults of format_down and color_down)
#
# Note: the network module requires PyPI package netifaces
# status.register("network",
#     interface="eth0",
#     format_up="{v4cidr}",)
# Note: requires both netifaces and basiciw (for essid and quality)

# status.register("cpu_usage",
#         format="\ue807 {usage}%",
#         # format_all="{usage}%",
#         color=COLORSCHEME["foreground"],
#         )

# Shows pulseaudio default sink volume
# Note: requires libpulseaudio from PyPI

status.register("pulseaudio",
        format="\ue809 {volume}%",
        color_unmuted=COLORSCHEME["foreground"],
        color_muted=COLORSCHEME["red"],
        sink='alsa_output.usb-Burr-Brown_from_TI_USB_Audio_CODEC-00.analog-stereo-output',
        )


status.register("xkblayout",
        color=COLORSCHEME['foreground'],
        # output_for_symbols={'colemak_egzvor_no_alt': 'us'},
        format = "\u2328 {symbol}"
        )

# status.register("mpd",
#         format="{artist} - {title} ({album}) {status}",
#         status={
#             "stop": "\ue80d",
#             "play": "\ue80c",
#             "pause": "\ue80e"
#         },
#         on_leftclick="switch_playpause",
#         on_rightclick="stop",
#         on_middleclick="toggle_hidden",
#         dummy_output="# ♪ #",
#         hide_inactive=True,
#         color=COLORSCHEME['foreground'],
#         color_map={
#             'play': COLORSCHEME['green'],
#             'pause': COLORSCHEME['cyan'],
#             'stop': COLORSCHEME['foreground'],
#         }
#     )

status.run()
