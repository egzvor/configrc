MTProxy
=======

Files
-----

- MTProxy.service -> /etc/systemd/system/MTProxy.service

- tgsecret -> (its content is an ``-S`` argument for ``mtproto-proxy``)

- proxy-secret -> path is ``--aes-pwd`` argument for ``mtproto-proxy``

- proxy-multi.conf -> path is a required ``config-file`` argument to ``mtproto-proxy``

- generate_link.sh -> script used to generate an invite link

Installation
------------

1. Clone MTProxy repository

2. Make a binary
