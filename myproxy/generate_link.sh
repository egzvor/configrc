if [ -z "$SERVER_NAME" ]; then
    echo "Specify SERVER_NAME variable";
    exit 1;
fi
if [ -z "$SECRET" ]; then
    echo "Specify secret";
    exit 1;
fi
echo "tg://proxy?server=${SERVER_NAME}&port=443&secret=${SECRET}"
