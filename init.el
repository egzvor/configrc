(require 'package)
(add-to-list 'package-archives (cons "melpa" "https://melpa.org/packages/") t)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("b89ae2d35d2e18e4286c8be8aaecb41022c1a306070f64a66fd114310ade88aa" default))
 '(helm-ag-base-command "rg --no-heading --smart-case")
 '(package-selected-packages
   '(## magit-stgit solarized-theme git-gutter jedi helm-rg magit use-package undo-tree org helm dracula-theme dired-du)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(package-initialize)
(setq inhibit-startup-screen t)
(setq tab-stop-list (number-sequence 4 120 4))
(setq auto-save-default nil)
(setq backup-directory-alist '(("." . "~/.emacs-saves")))

(push (cons "\\*shell\\*" display-buffer--same-window-action) display-buffer-alist)

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(add-to-list 'default-frame-alist '(font . "Hack" ))
(global-display-line-numbers-mode)
(global-git-gutter-mode 1)
(show-paren-mode 1)

(eval-when-compile
  (require 'use-package))
(use-package jedi
  :config (add-hook 'python-mode-hook 'jedi:setup))
(use-package helm
  :bind (("M-a" . helm-M-x)
         ("C-x C-f" . helm-find-files)
         ("C-SPC" . helm-dabbrev))
  :config (progn
            (setq helm-mode-fuzzy-match t)
	    (setq helm-M-x-fuzzy-match t)
            (setq helm-completion-in-region-fuzzy-match t)
            (helm-mode 1)))

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
