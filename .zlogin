# Randomly choosing welcome sign
c=$[${RANDOM}%2]
case $c in
    0)
        IFS='' read -r -d '' name <<-'EOF'
    _/_/_/_/            _/_/_/_/_/
   _/          _/_/_/        _/    _/      _/    _/_/    _/  _/_/
  _/_/_/    _/    _/      _/      _/      _/  _/    _/  _/_/
 _/        _/    _/    _/          _/  _/    _/    _/  _/
_/_/_/_/    _/_/_/  _/_/_/_/_/      _/        _/_/    _/
               _/
          _/_/
EOF
    ;;
    1)
        IFS='' read -r -d '' name <<-'EOF'
 _______   _______  ________  ____    ____  ______   .______
|   ____| /  _____||       /  \   \  /   / /  __  \  |   _  \
|  |__   |  |  __  `---/  /    \   \/   / |  |  |  | |  |_)  |
|   __|  |  | |_ |    /  /      \      /  |  |  |  | |      /
|  |____ |  |__| |   /  /----.   \    /   |  `--'  | |  |\  \----.
|_______| \______|  /________|    \__/     \______/  | _| `._____|
EOF
    ;;
esac
echo "Welcome,"
echo "$name"
date
# echo "Type startx to run i3 window manager.."
pgrep "startx" > /dev/null 2>&1 || ssh-agent startx
