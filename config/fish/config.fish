if status is-login
    if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
        if test -z (pgrep ssh-agent)
            eval (ssh-agent -c)
            set -Ux SSH_AUTH_SOCK $SSH_AUTH_SOCK
            set -Ux SSH_AGENT_PID $SSH_AGENT_PID
            set -Ux SSH_AUTH_SOCK $SSH_AUTH_SOCK
        end
        exec startx -- -keeptty -ardelay 250 -arinterval 10
    end
    pyenv init --path | source
end

if status is-interactive
    pyenv init - | source
end

direnv hook fish | source
zoxide init fish | source
source /opt/asdf-vm/asdf.fish
