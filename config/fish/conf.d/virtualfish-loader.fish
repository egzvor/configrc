set -g VIRTUALFISH_VERSION 2.5.5
set -g VIRTUALFISH_PYTHON_EXEC /home/egzvor/.local/pipx/venvs/virtualfish/bin/python
source /home/egzvor/.local/pipx/venvs/virtualfish/lib/python3.10/site-packages/virtualfish/virtual.fish
emit virtualfish_did_setup_plugins