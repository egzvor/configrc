# This module allows you to switch to a different task
# when an interactive command takes too long
# by notifying you when it is finished.

function __postexec_notify_on_long_running_commands --on-event fish_postexec
    # Preserve the status of the long running command.
    set command_status $status

    # Go up the parent ids chain until X window is found.
    set ppid $fish_pid
    while true
        set ppid (ps -o ppid:1= $ppid)
        if test $ppid -eq 1
            break
        end
        set wid (xdotool search --pid $ppid 2>/dev/null)
        if string length -q "$wid"
            break
        end
    end

    # Do not notify if process' window is active.
    if string length -q "$wid"; and test "$(xdotool getactivewindow 2>/dev/null)" = "$wid"
        return
    end

    if test $command_status -ne 0
        set urgency critical
    else
        set urgency normal
    end

    "$scripts/notify-with-jump.fish" "$argv" "$urgency" "$fish_pid" < /dev/null &>/dev/null &; disown
end
