function review-start
    if test (count $argv) -ne 2
        echo 'Supply `from` and `to` SHA1' >&2
        return 2
    end

    set --universal PR_FROM_SHA $argv[1]
    set --universal PR_TO_SHA $argv[2]
    set --universal PR_GIT_DIR (realpath (git rev-parse --git-dir))

    git log ^$PR_FROM_SHA $PR_TO_SHA --no-merges --format=format:%H \
        | xargs -I{} kitty --detach --directory {}      \
          vim                                           \
            -n                                          \
            -R                                          \
            --servername {}                             \
            -c 'set switchbuf+=useopen,usetab | GlistDiff'
end
