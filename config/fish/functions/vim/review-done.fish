function review-done
    if test -z "$PR_FROM_SHA"
        or test -z "$PR_TO_SHA"
        or test -z "$PR_GIT_DIR"

        echo 'Missing PR_* variables, run review-start first!'
        return 2
    end

    git log ^$PR_FROM_SHA $PR_TO_SHA --no-merges --format=format:%H \
        | xargs -I{} \
          vim --servername {} --remote-send '<C-\\><C-N>:quitall<CR>'

    set --erase --universal PR_FROM_SHA
    set --erase --universal PR_TO_SHA
    set --erase --universal PR_GIT_DIR
end
