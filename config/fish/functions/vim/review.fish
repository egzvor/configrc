function review
    if test (count $argv) -ne 1
        set ref "HEAD~"
    else
        set ref $argv[1]
    end
    GIT_EDITOR="VIM_LIGHT=true vim --servername review -n -R -M -c \"let g:diff_base='$ref'\"" git jump diff --no-ext-diff $ref
end
