function v --wraps vim
    VIM_LIGHT=true V $argv
end
