function V --wraps vim
    # Open existing session if called without args
    if test (count $argv) -eq 0; and test -f Session.vim
        set argv $argv -S Session.vim
    end

    # Always specify a servername
    if not contains -- '--servername' $argv
        set servername (path change-extension '' (path basename (path dirname $PWD)))/(path basename $PWD)
        set argv $argv --servername $servername
    end

    command vim $argv
end
