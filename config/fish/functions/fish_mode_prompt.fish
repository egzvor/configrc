# The fish_mode_prompt function is prepended to the prompt
function fish_mode_prompt --description "Displays the current mode"
    # Do nothing if not in vi mode
    if test "$fish_key_bindings" != fish_vi_key_bindings
        and test "$fish_key_bindings" != fish_hybrid_key_bindings
    end

    switch $fish_bind_mode
        case insert
            return
        case default
            set_color --bold red
            echo '[N]'
        case replace_one
            set_color --bold green
            echo '[R]'
        case replace
            set_color --bold cyan
            echo '[R]'
        case visual
            set_color --bold magenta
            echo '[V]'
    end
    set_color normal
    echo -n ' '
end
