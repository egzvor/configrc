function gwd
    set to_delete (git worktree list | cut -f1 -d' ' | fzf --multi)
    if test (count $to_delete) -eq 0
        return
    end

    for w in $to_delete
        git worktree remove "$w"
    end
end
