function git_aliases
    alias g "git"
    alias gst "git status"
    alias gb "git branch"
    alias gbc "echo Deleting branches:;git branch | fzf -m | awk '{print \$1}' | xargs git branch -D"
    alias gbd "git_delete_branches"
    alias gd "git diff"
    alias gf "git fetch"
    alias gca "git commit --amend"
    alias gaa "git add -A; git status"
    alias gco "git checkout"
    alias gr "git rebase"
    alias gl "git log"
    alias gl1 "git log -1"
    alias gll "git log --oneline"
    alias grh "git reset --hard"
    alias gs "git switch"
    alias gj "git jump"
    alias gjl "git jump diff --no-ext-diff HEAD^"
    alias gh "git hist"
    alias gw 'git worktree'
    alias gwl 'git worktree list'
    alias gu 'gitui'
end
