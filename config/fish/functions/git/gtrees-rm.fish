function gtrees-rm
    if test (count $argv) -ne 2;
        echo 'Supply `from` and `to` SHA1' >&2
        return 2
    end

    git log ^$argv[1] $argv[2] --no-merges --format=format:%H \
        | xargs -L1 git worktree remove
end
