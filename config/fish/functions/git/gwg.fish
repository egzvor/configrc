function gwg
    set to (git worktree list | cut -f1 -d' ' | fzy)
    if test -n "$to"
        cd "$to"
    end
end
