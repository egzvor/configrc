function feh
    command feh --auto-zoom --scale-down $argv
end
