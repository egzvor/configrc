function fish_prompt
    set -l __last_command_exit_status $status

    if set -q VIRTUAL_ENV
        echo -n -s (set_color -b normal blue) "(" (basename "$VIRTUAL_ENV") ")" (set_color normal) " "
    end

    if not set -q -g __fish_robbyrussell_functions_defined
        set -g __fish_robbyrussell_functions_defined
        function _git_branch_name
            set -l branch (git symbolic-ref --quiet HEAD 2>/dev/null)
            if set -q branch[1]
                echo (string replace -r '^refs/heads/' '' $branch)
            else
                echo (git rev-parse --short HEAD 2>/dev/null)
            end
        end

        function _is_git_dirty
            echo (git status -s --ignore-submodules=dirty 2>/dev/null)
        end

        function _is_git_repo
            type -q git
            or return 1
            git rev-parse --git-dir >/dev/null 2>&1
        end

        function _repo_branch_name
            _$argv[1]_branch_name
        end

        function _is_repo_dirty
            _is_$argv[1]_dirty
        end

        function _repo_type
            if _is_git_repo
                echo 'git'
                return 0
            end
            return 1
        end
    end

    set -l cyan (set_color cyan)
    set -l yellow (set_color yellow)
    set -l red (set_color red)
    set -l green (set_color green)
    set -l blue (set_color blue)
    set -l normal (set_color normal)

    if test $__last_command_exit_status != 0
        if test "$USER" = 'root'
            set arrow "$red# "
        else
            set arrow "😿"
        end
    else
        if test "$USER" = 'root'
            set arrow "$normal# "
        else
            set arrow "🐱"
        end
    end

    if jobs --query
        set job_indicator '⚡'
    else
        set job_indicator ''
    end

    if set -l repo_type (_repo_type)
        set -l repo_branch (_repo_branch_name $repo_type)
        set repo_info "$cyan($repo_branch)"

        if [ (_is_repo_dirty $repo_type) ]
            set -l dirty "$yellow ✗"
            set repo_info "$repo_info$dirty"
        end
    end

    echo -s $blue (fish_prompt_pwd_dir_length=0 prompt_pwd)
    echo -n -s $arrow $job_indicator $repo_info $normal ' '
end
