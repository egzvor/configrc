function fish_user_key_bindings
    # Execute this once per mode that emacs bindings should be used in
    fish_default_key_bindings -M insert
    # Then execute the vi-bindings so they take precedence when there's a conflict.
    # Without --no-erase fish_vi_key_bindings will default to
    # resetting all bindings.
    # The argument specifies the initial mode (insert, "default" or visual).
    fish_vi_key_bindings --no-erase insert

    git_aliases
    core_utils_aliases
    vim_aliases
    other_aliases

    fzf_key_bindings
    fzy_key_bindings
    bind -M insert \cw backward-kill-bigword
    bind -M insert \cz 'fg 2>/dev/null; commandline -f repaint'

    function _escape_commandline
        commandline (string escape (commandline))
    end
    bind -M insert \cs _escape_commandline
end
