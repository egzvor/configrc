function symlink-fish-functions
    find . -mindepth 2 -type f -exec ln -sf '{}' ./ \;
end
