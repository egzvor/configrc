function godeps
    rg '\t' go.mod | rg -v 'indirect' | tr '[:blank:]' '\t' | cut -f2,3 | sed -E "s#(\S+)\t(\S+)#$(go env GOPATH)/pkg/mod/\1@\2#" | sed -E 's/[A-Z]/!\L\0/g'
end
