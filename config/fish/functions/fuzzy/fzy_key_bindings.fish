function fzy_key_bindings
# Store current token in $dir as root for the 'find' command
    function fzy-file-widget -d "List files and folders"
set -l commandline (__fzy_parse_commandline)
    set -l dir $commandline[1]
    set -l fzy_query $commandline[2]

# "-path \$dir'*/\\.*'" matches hidden files/folders inside $dir but not
# $dir itself, even if hidden.
    test -n "$FZY_CTRL_T_COMMAND"; or set -l FZY_CTRL_T_COMMAND "
    command find -L \$dir -mindepth 1 \\( -path \$dir'*/\\.*' -o -fstype 'sysfs' -o -fstype 'devfs' -o -fstype 'devtmpfs' \\) -prune \
        -o -type f -print \
        -o -type d -print \
        -o -type l -print 2> /dev/null | sed 's@^\./@@'"

        test -n "$FZY_TMUX_HEIGHT"; or set FZY_TMUX_HEIGHT 40%
        begin
        set -lx FZY_DEFAULT_OPTS "--height $FZY_TMUX_HEIGHT --reverse --bind=ctrl-z:ignore $FZY_DEFAULT_OPTS $FZY_CTRL_T_OPTS"
        # eval "$FZY_CTRL_T_COMMAND | "(__fzycmd)' -m --query "'$fzy_query'"' | while read -l r; set result $result $r; end
        rg --no-ignore --files --hidden --glob '!.git/' | fzy --query "$fzy_query" | while read -l r; set result $result $r; end
        end
        if [ -z "$result" ]
        commandline -f repaint
        return
        else
# Remove last token from commandline.
        commandline -t ""
        end
    for i in $result
commandline -it -- (string escape $i)
    commandline -it -- ' '
    end
    commandline -f repaint
    end

    function __fzy_parse_commandline -d 'Parse the current command line token and return split of existing filepath and rest of token'
        # eval is used to do shell expansion on paths
        set -l commandline (eval "printf '%s' "(commandline -t))

        if [ -z $commandline ]
          # Default to current directory with no --query
          set dir '.'
          set fzy_query ''
        else
          set dir (__fzy_get_dir $commandline)

          if [ "$dir" = "." -a (string sub -l 1 -- $commandline) != '.' ]
            # if $dir is "." but commandline is not a relative path, this means no file path found
            set fzy_query $commandline
          else
            # Also remove trailing slash after dir, to "split" input properly
            set fzy_query (string replace -r "^$dir/?" -- '' "$commandline")
          end
        end

        echo $dir
        echo $fzy_query
      end

      function __fzy_get_dir -d 'Find the longest existing filepath from input string'
        set dir $argv

        # Strip all trailing slashes. Ignore if $dir is root dir (/)
        if [ (string length -- $dir) -gt 1 ]
          set dir (string replace -r '/*$' -- '' $dir)
        end

        # Iteratively check if dir exists and strip tail end of path
        while [ ! -d "$dir" ]
          # If path is absolute, this can keep going until ends up at /
          # If path is relative, this can keep going until entire input is consumed, dirname returns "."
          set dir (dirname -- "$dir")
        end

        echo $dir
      end

    bind \ct fzy-file-widget
    if bind -M insert > /dev/null 2>&1
        bind -M insert \ct fzy-file-widget
    end
end
