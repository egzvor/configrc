function dk
    if test (count $argv) -gt 0
        docker $argv
    else
        docker ps -a --format="table {{.ID}}\t{{.Names}}\t{{.Image}}\t{{.Command}}\t{{.Status}}\t{{.Ports}}" | less -S
    end
end
