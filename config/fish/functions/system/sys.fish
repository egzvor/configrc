function sys
    if test (count $argv[2..]) -gt 0
        sudo systemctl $argv[2] $argv[1]
    else
        sudo systemctl start $argv[1]
    end
end
