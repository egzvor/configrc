function enable-touchpad
 xinput set-prop (xinput | perl -n -e '/touchpad.*id=(\d+)/i && print $1') 'libinput Tapping Enabled' 1; 
end
