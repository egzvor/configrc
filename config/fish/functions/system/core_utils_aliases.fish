function core_utils_aliases
    alias ls='exa'
    alias ll='exa --long --group-directories-first --color=always'
    alias la='exa --long --all --group-directories-first --color=always'
    alias lt='exa --long --sort=modified --reverse --color=always'
    alias oct='stat -c "%a %n"'
    alias cal="cal -m -3"
    alias timestamp='date +%s'
    alias fzf="fzf --color light"
end
