-- set confirm
-- set expandtab softtabstop=4 tabstop=8 shiftwidth=4
-- set fillchars=vert:│,fold:┈,diff:┈
-- set foldcolumn=0 foldlevelstart=99
-- set incsearch ignorecase smartcase
-- set lazyredraw
-- set list listchars=tab:··
-- set mouse=a
-- set pastetoggle=<F2>
-- set noruler noshowcmd
-- set shortmess+=I
-- set splitbelow splitright
-- set tags=./tags;$HOME,tags
-- set ttyfast
-- set undofile
-- set viminfofile=$HOME/.local/share/nvim/shada/main.shada
-- set wildcharm=<c-z>
-- set wildignorecase
-- set wildmode=list:lastused,full

-- set keymap=russian-jcukenwin iminsert=0 imsearch=-1 spelllang=en_gb,ru_ru

-- command! Vimrc noswapfile view +set\ nomodifiable $MYVIMRC
-- command! SVimrc <mods> noswapfile sview +set\ nomodifiable $MYVIMRC
-- command! FtPlugin execute !empty(&filetype) ? 'e $HOME/.vim/after/ftplugin/' . &filetype . '.vim' : 'echo "No filetype"'
-- command! -nargs=* Terminal <mods> split | lcd %:h | terminal ++curwin <args>
-- command! -range=% Ybuffer <line1>,<line2>y+
-- command! Ypath let @+=expand('%')
-- command! Server echo "Server: " . v:servername

-- -- Mappings {{{
-- let mapleader="\<space>"
-- let maplocalleader = ","

-- nnoremap <space> <nop>
-- nnoremap \ ,
-- xnoremap \ ,
-- onoremap \ ,

-- -- Window and tab management
-- nnoremap <c-l> <c-w>l
-- nnoremap <c-h> <c-w>h
-- nnoremap <c-j> <c-w>j
-- nnoremap <c-k> <c-w>k
-- nnoremap <c-w>t <cmd>tab split<cr>
-- nnoremap <c-w>c <cmd>tabclose<cr>
-- nnoremap <c-w>O <cmd>tabonly<cr>
-- nnoremap L gt
-- nnoremap H gT

-- -- Improving default key bindings
-- nnoremap Y y$
-- noremap ` '
-- noremap ' `
-- noremap '' ``
-- noremap `` ''
-- cnoremap <expr> <c-p> wildmenumode() ? '<c-p>' : '<up>'
-- cnoremap <expr> <c-n> wildmenumode() ? '<c-n>' : '<down>'
-- cnoremap <up> <c-p>
-- cnoremap <down> <c-n>
-- -- Break insert change when making a new line.
-- -- That way each inserted line is a different change that you can undo, without
-- -- loosing all the lines typed before the last one.
-- inoremap <cr> <c-]><c-g>u<cr>

-- -- Adding new behaviour
-- -- Go to previous loaded buffer
-- nnoremap <silent> <bs> :<c-u>exe v:count ? v:count . 'b' : 'b' . (bufloaded(0) ? '#' : 'n')<cr>
-- nnoremap <leader>Q <cmd>q!<cr>

-- -- Search and replace
-- nnoremap gs :%s/\<<c-r><c-w>\>//g<left><left>
-- nnoremap c* *``cgn
-- nnoremap c# #``cgN
-- xnoremap <leader>* y/\V\C<c-r>"<cr>``
-- xnoremap <leader># y?\V\C<c-r>"<cr>``
-- nnoremap <leader>* /\<<c-r><c-w>\>\C<cr>
-- nnoremap <leader># ?\<<c-r><c-w>\>\C<cr>
-- nnoremap <leader>/ `</\%V
-- xnoremap <leader>/ <esc>`</\%V
-- nnoremap <leader>? `>?\%V
-- xnoremap <leader>? <esc>`>?\%V

-- -- Next incremental search match with Tab
-- function! IsCmdTypeSearch()
--     return getcmdtype() == "/" || getcmdtype() == "?"
-- endfunction
-- cnoremap <expr> <tab>   IsCmdTypeSearch() ? "<c-g>" : "<c-z>"
-- cnoremap <expr> <s-tab> IsCmdTypeSearch() ? "<c-t>" : "<s-tab>"

-- -- File navigation
-- nnoremap <leader>e :edit <c-r>=expand("%:.:h") . "/"<cr><c-z>
-- nnoremap <leader>f :edit **/

-- -- Run Vifm bound to the current Vim server
-- function! Vifm() abort
--     if v:servername == ''
--         echo 'Run vim with --servername first!'
--     else
--         execute 'silent !urxvtc -e vifm --server-name ' . v:servername . ' ' . getcwd() . ' -c :tree'
--     endif
-- endfunction

-- command Vifm call Vifm()

-- function! QuickFixTree(location=0) abort
--     if a:location == 1
--         let entries = getloclist(0)
--         let list_name = "Location"
--     else
--         let entries = getqflist()
--         let list_name = "Quickfix"
--     endif

--     if empty(entries)
--         echo list_name . " list is empty"
--         return
--     endif

--     let paths = map(entries, {idx, entry -> fnamemodify(bufname(entry['bufnr']), ":p:.")})
--     echo system("tree -a --fromfile .", paths)
-- endfunction

-- command! TreeLoc call QuickFixTree(1)
-- command! TreeQuick call QuickFixTree(0)

-- -- QuickFix management
-- nnoremap <leader>ca <cmd>caddexpr expand("%") . ":" . line(".") .  ":" . getline(".")<cr>
-- nnoremap <leader>cc <cmd>call setqflist([])<cr>
-- nnoremap <leader>la <cmd>laddexpr expand("%") . ":" . line(".") .  ":" . getline(".")<cr>
-- nnoremap <leader>lc <cmd>call setloclist(".", [])<cr>
-- command -nargs=0 QuickFixToLocList call setloclist(".", getqflist())
-- command -nargs=0 LocListToQuickFix call setloclist(".", getqflist())

-- -- Buffer navigation
-- nnoremap <leader>b :buffers<cr>:buffer<space>
-- nnoremap <leader><c-o> :jumps<cr>:normal <c-o><s-left>
-- nnoremap <leader><c-i> :jumps<cr>:normal <c-i><s-left>
-- -- nnoremap <leader>m :marks<cr>:normal '
-- nnoremap <leader>m :tab term make<space>
-- nnoremap <leader>t <cmd>tag<cr>
-- nnoremap <leader>T :tjump<space>

-- -- Yanking and pasting
-- xnoremap <leader>y "+y
-- nnoremap <leader>y "+y
-- nnoremap <leader>yy "+yy
-- nnoremap <leader>p "+
-- xnoremap <leader>p "+
-- inoremap <expr> <c-p> pumvisible() ? '<c-p>' : '<c-r>+'

-- -- For grep commands
-- -- c-@ is ctrl-space
-- cnoremap <c-@> .*

-- -- Show the syntax highlight group under cursor
-- nnoremap <F10> <cmd>echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
--     \ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
--     \ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<cr>

-- -- Don't include leading whitespace
-- onoremap a' 2i'
-- onoremap a" 2i"
-- xnoremap a' 2i'
-- xnoremap a" 2i"

-- -- Mappings with external dependencies
-- nnoremap <leader>o :bdelete <bar> silent !vims #:p<cr>:redraw!<cr>
-- nnoremap <leader>O :bdelete <bar> silent !vimt #:p<cr>:redraw!<cr>
-- nnoremap <leader>u <cmd>silent w !urlview<cr>
-- nnoremap <leader>d <cmd>w !diff -u % -<cr>

-- inoreabbrev teh the
-- inoreabbrev wihch which
-- inoreabbrev waht what
-- }}}

-- runtime plugin/mycolors.vim
-- colorscheme solarized
-- set background=light

-- require('plugins')
-- -- Setup nvim-cmp.
-- local cmp = require'cmp'

-- cmp.setup({
-- snippet = {
--   -- REQUIRED - you must specify a snippet engine
--   expand = function(args)
--     require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
--   end,
-- },
-- mapping = {
--   ['<C-b>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
--   ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
--   ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
--   ['<C-y>'] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
--   ['<C-e>'] = cmp.mapping({
--     i = cmp.mapping.abort(),
--     c = cmp.mapping.close(),
--   }),
--   ['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
-- },
-- sources = cmp.config.sources({
--   { name = 'nvim_lsp' },
--   -- { name = 'vsnip' }, -- For vsnip users.
--   { name = 'luasnip' }, -- For luasnip users.
--   -- { name = 'ultisnips' }, -- For ultisnips users.
--   -- { name = 'snippy' }, -- For snippy users.
-- }, {
--   { name = 'buffer' },
-- })
-- })

-- -- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
-- cmp.setup.cmdline('/', {
-- sources = {
--   { name = 'buffer' }
-- }
-- })

-- -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
-- cmp.setup.cmdline(':', {
-- sources = cmp.config.sources({
--   { name = 'cmdline' , keyword_length = 4 }
-- }, {
--   { name = 'path' }
-- })
-- })

-- -- Setup lspconfig.
-- require('lspconfig').gopls.setup{}
-- local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
-- -- Replace <YOUR_LSP_SERVER> with each lsp server you've enabled.
-- -- require('lspconfig')['<YOUR_LSP_SERVER>'].setup {
-- require('lspconfig')['gopls'].setup {
-- capabilities = capabilities
-- }

-- local function prequire(...)
-- local status, lib = pcall(require, ...)
-- if (status) then return lib end
--   return nil
-- end

-- local luasnip = prequire('luasnip')
-- local cmp = prequire("cmp")

-- local t = function(str)
--   return vim.api.nvim_replace_termcodes(str, true, true, true)
-- end

-- local check_back_space = function()
--   local col = vim.fn.col('.') - 1
--   if col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') then
--       return true
--   else
--       return false
--   end
-- end

-- _G.tab_complete = function()
--   if cmp and cmp.visible() then
--       cmp.select_next_item()
--   elseif luasnip and luasnip.expand_or_jumpable() then
--       return t("<Plug>luasnip-expand-or-jump")
--   elseif check_back_space() then
--       return t "<Tab>"
--   else
--       cmp.complete()
--   end
--   return ""
-- end
-- _G.s_tab_complete = function()
--   if cmp and cmp.visible() then
--       cmp.select_prev_item()
--   elseif luasnip and luasnip.jumpable(-1) then
--       return t("<Plug>luasnip-jump-prev")
--   else
--       return t "<S-Tab>"
--   end
--   return ""
-- end

-- vim.api.nvim_set_keymap("i", "<Tab>", "v:lua.tab_complete()", {expr = true})
-- vim.api.nvim_set_keymap("s", "<Tab>", "v:lua.tab_complete()", {expr = true})
-- vim.api.nvim_set_keymap("i", "<S-Tab>", "v:lua.s_tab_complete()", {expr = true})
-- vim.api.nvim_set_keymap("s", "<S-Tab>", "v:lua.s_tab_complete()", {expr = true})
-- vim.api.nvim_set_keymap("i", "<C-E>", "<Plug>luasnip-next-choice", {})
-- vim.api.nvim_set_keymap("s", "<C-E>", "<Plug>luasnip-next-choice", {})
