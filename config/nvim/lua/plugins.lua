-- -- Only required if you have packer configured as `opt`
-- -- vim.cmd [[packadd packer.nvim]]

-- return require('packer').startup(function()
--   -- Packer can manage itself
--   use 'wbthomason/packer.nvim'

--   use 'antoineco/vim-colors-solarized'

--   use 'hrsh7th/cmp-buffer'
--   use 'hrsh7th/cmp-cmdline'
--   use 'hrsh7th/cmp-nvim-lsp'
--   use 'hrsh7th/cmp-path'
--   use 'hrsh7th/nvim-cmp'

--   use 'neovim/nvim-lspconfig'

--   use 'bitc/vim-bad-whitespace'
--   use 'dag/vim-fish'
--   use {
--       'EgZvor/vim-unimpaired', branch = 'egzvor'
--   }
--   use 'flwyd/vim-conjoin'
--   use 'jeetsukumaran/vim-indentwise'
--   use 'jidn/vim-dbml'
--   use 'justinmk/vim-dirvish'
--   use 'lifepillar/pgsql.vim'
--   use 'machakann/vim-sandwich'
--   use 'majutsushi/tagbar'
--   use 'RRethy/vim-illuminate'
--   use 'simnalamburt/vim-mundo'
--   use 'stefandtw/quickfix-reflector.vim'
--   use 'tommcdo/vim-exchange'
--   use 'tpope/vim-abolish'
--   use 'tpope/vim-commentary'
--   use 'tpope/vim-eunuch'
--   use 'tpope/vim-obsession'
--   use 'tpope/vim-projectionist'
--   use 'tpope/vim-repeat'
--   use 'tweekmonster/startuptime.vim'
--   use 'L3MON4D3/LuaSnip'
-- end)
