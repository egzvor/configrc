Configuration files
===================

Where to put what
-----------------

- ``hwdb.d/`` to ``/usr/lib/udev/hwdb.d/``

- Use ``install_home.sh`` and ``install_config.py``

What else to install
--------------------

- urxvt perl extensions:

  - keyboard-select

  - resize-font
