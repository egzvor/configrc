#!/usr/bin/env python

from contextlib import suppress
from pathlib import Path
import shutil

for app_config in Path("config").iterdir():
    with suppress(FileExistsError):
        Path("~/.config/", app_config.name).expanduser().symlink_to(
            app_config.resolve()
        )
