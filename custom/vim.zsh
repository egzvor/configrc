# Pipe this to commands that output list of files
# Example: grep -nr -e 'kek' -l | vimargs
function vimargs() {
    xargs -o | vim
}

alias vimconfig="vim --servername config \"+cd ~/.vim\" -- ~/.vim/vimrc"
# alias vim="vim -w ~/.vim.log"
