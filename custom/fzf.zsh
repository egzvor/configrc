source /usr/share/fzf/key-bindings.zsh
# source /usr/share/fzf/completion.zsh

# Rebind default backward search
bindkey '^R' history-incremental-search-backward
# Override fzf default behaviour
bindkey '^X^R' fzf-history-widget

# alias fzf='fzf '
# export FZF_DEFAULT_COMMAND='fd --type f'  # I use fzy for file exploration.
export FZF_DEFAULT_OPTS='--color light --border --height 40%'
