# CTRL-T - Paste the selected file path(s) into the command line
__fsel() {
  setopt localoptions pipefail 2> /dev/null
  rg --files --hidden --glob '!.git/' | fzy
  local ret=$?
  echo
  return $ret
}

fzy-file-widget() {
  LBUFFER="${LBUFFER}$(__fsel)"
  local ret=$?
  zle reset-prompt
  return $ret
}
zle     -N   fzy-file-widget
bindkey '^T' fzy-file-widget

# CTRL-R - Paste the selected command from history into the command line
# fzy-history-widget() {
#   local selected num
#   setopt localoptions noglobsubst noposixbuiltins pipefail 2> /dev/null
#   selected=($(fc -rl 1 | fzy))
#   local ret=$?
#   if [ -n "$selected" ]; then
#     num=$selected[1]
#     if [ -n "$num" ]; then
#       zle vi-fetch-history -n $num
#     fi
#   fi
#   zle reset-prompt
#   return $ret
# }
# zle     -N   fzy-history-widget
# bindkey '^X^R' fzy-history-widget
