# Dracula Theme v1.2.5
#
# https://github.com/dracula/dracula-theme
#
# Copyright 2016, All rights reserved
#
# Code licensed under the MIT license
# http://zenorocha.mit-license.org
#
# @author Zeno Rocha <hi@zenorocha.com>

local ret_status="%(?:%{$fg[default]%}$:%{$fg[red]%}$)"

PROMPT='${ret_status}%{$fg_bold[blue]%} $(git_prompt_info)% %{$reset_color%}'

MODE_INDICATOR="%{$fg_bold[red]%}<<<%{$reset_color%}"

RPROMPT='%{$fg_bold[blue]%}%~%{$reset_color%} $(vi_mode_prompt_info)'

ZSH_THEME_GIT_PROMPT_CLEAN=") %{$fg_bold[green]%}✔ "
ZSH_THEME_GIT_PROMPT_DIRTY=") %{$fg_bold[yellow]%}✗ "
ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[cyan]%}("
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"

# vim: ft=zsh
