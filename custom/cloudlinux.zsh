#/usr/bin/zsh

alias cl="cd ~/CloudLinux"
alias clc="cd ~/CloudLinux/securelve"
alias cld="cd ~/CloudLinux/accesssupport/cldoctor/doctor.cloudlinux.com/"
alias cll="cd ~/CloudLinux/python-cllib"
alias clm="cd ~/CloudLinux/cpanel-lvemanager"
alias clh="cd ~/CloudLinux/accesssupport/share-ssh"
alias clz="cd ~/CloudLinux/accesssupport/doctor_zen"
alias clp="cd ~/CloudLinux/python_lve"
alias clr="cd ~/CloudLinux/rhn-client-tools"
alias cls="cd ~/CloudLinux/lve-stats"
alias clq="cd ~/CloudLinux/QA"
alias cqc="cd ~/CloudLinux/QA/rpm_tests/p_cagefs"
alias cqm="cd ~/CloudLinux/QA/rpm_tests/p_lvemanager"
alias cqp="cd ~/CloudLinux/QA/rpm_tests/p_lve-utils"

alias im="cd ~/Imunify360"
alias ima="cd ~/Imunify360/aibolit"
alias imb="cd ~/Imunify360/cloudlinux-backup-utils"
alias imd="cd ~/Imunify360/defence360"
alias imr="cd ~/Imunify360/i360-realtime-av"
alias ims="cd ~/Imunify360/safe_ops"
alias glt='gl --author "Egor Zvorykin\|Sergey Koziniy\|Maksim Kanushin\|Alexander Tishin"'
