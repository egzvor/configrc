alias ls='exa'
alias ll='exa --long --group-directories-first'
alias la='exa --long --all --group-directories-first'
alias lt='exa --long --sort=modified --reverse'
alias oct='stat -c "%a %n"'
alias cal="cal -m"
alias timestamp='date +%s'
alias signals='for i in $(kill -l); do echo $i; done | nl'

# chpwd is run every time we change directory (cd, pushd, popd, etc.)
chpwd() ls
