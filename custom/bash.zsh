# Simulating some default bash bindings

bindkey "^F" forward-char
bindkey "^B" backward-char
bindkey "^S" history-incremental-search-forward
bindkey "^U" backward-kill-line
