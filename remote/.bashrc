# .bashrc

# User specific aliases and functions

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

# Disable flow control
stty -ixon

# Source global definitions
if [ -f /etc/bashrc ]; then
	source /etc/bashrc
fi

PROMPT_COMMAND=""
EDITOR="vim"
